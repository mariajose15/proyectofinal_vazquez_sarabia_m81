#include <Servo.h>

Servo myservo;

int hzone1, hzone2, hzone3, hzone4, hzone5;
int humesum;
float Hprom;

int bomba = 12;

int tempA;

String cadTX = "";

void setup() {

  pinMode(bomba, OUTPUT);
  myservo.attach(9);

  Serial.begin(9600);

}

void loop() {

  lecturasH();

  humesum = hzone1 + hzone2 + hzone3 + hzone4 + hzone5;
  Hprom = humesum / 5;

  if ( Hprom < 15 )
  {
    myservo.write(90);
    delay(1000);
    digitalWrite(bomba, HIGH);
  }

  if ( Hprom >= 95 )

  {
    digitalWrite(bomba, LOW);
    delay(1000);
    myservo.write(0);
  }

  temp();

  cadena(); 

}

void lecturasH () {

  int lecturaZ1 = analogRead(A0);
  hzone1 = map(lecturaZ1, 1023, 0, 0, 100);

  int lecturaZ2 = analogRead(A1);
  hzone2 = map(lecturaZ2, 1023, 0, 0, 100);

  int lecturaZ3 = analogRead(A2);
  hzone3 = map(lecturaZ3, 1023, 0, 0, 100);

  int lecturaZ4 = analogRead(A3);
  hzone4 = map(lecturaZ4, 1023, 0, 0, 100);

  int lecturaZ5 = analogRead(A4);
  hzone5 = map(lecturaZ5, 1023, 0, 0, 100);

}

void temp() {

  float analogvalue = analogRead(A5);
  float voltagevalue = analogvalue * 5.0 / 1023.0;

  tempA = (voltagevalue - 0.5) * 100;

}

void cadena() {

  cadTX = cadTX + "~/";
  cadTX += Hprom;
  cadTX += "/";
  cadTX += tempA;
  cadTX += "/";
  cadTX += digitalRead(bomba);
  cadTX += "/";
  cadTX += hzone1;
  cadTX += "/";
  cadTX += hzone2;
  cadTX += "/";
  cadTX += hzone3;
  cadTX += "/";
  cadTX += hzone4;
  cadTX += "/";
  cadTX += hzone5;
  cadTX += "/";

  Serial.print(cadTX);
  cadTX = "";


}